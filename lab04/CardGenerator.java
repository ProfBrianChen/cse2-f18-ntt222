// Ngan Tran - CSE002

// I am a magician and I need to practice my card tricks before my big show in Las Vegas.
//Since I don't want to reveal my secrets, I need to write a program 
//that pick a random card from 1 to 52

public class CardGenerator{
  //main method required for every Java program
  public static void main (String [] args) {
    int card = (int) (Math.random()*52)+1;
    
    String suit;
    int remainder = card % 13;
    
    if ((card >= 1) && (card <= 13)) {
        suit = "Diamonds";
    } else if ((card >= 14) && (card <= 26)) {
      suit = "Clubs";
    } else if ((card >=27) && (card <= 39)) {
      suit = "Hearts";
    } else {
      suit = "Spades"; 
    }
    
    
    
    switch (remainder) {
      case 1:
        System.out.println("You picked the Ace of " + suit);
        break;
      case 0: 
        System.out.println("You picked the King of " + suit);
        break;
      case 12:
        System.out.println("You picked the Queen of " + suit);
        break;
      case 11:
        System.out.println("You picked the Jack of " + suit);
        break;
      default: 
        System.out.println("You picked the " + remainder + "of " + suit);
        break;
        }
    
    
  }
}