// Ngan Tran - CSE002  - Section 210

import java.util.Scanner;

/* Developing a program to hide the secret message X. 
Instead of printing out X which everyone can see clearly, we will bury the character in a handful of stars.
*/

public class EncryptedX {
  
  public static void main(String[] arg) {
    
    Scanner scan = new Scanner(System.in);
    
    System.out.print("Please enter a number from 0 to 100: ");
    String junk;
    
    // Check if the input is valid
    while (!scan.hasNextInt()) {
      System.out.print("Not valid input. Please enter a number from 0 to 100 again: ");
      junk = scan.nextLine();
    }
     int input = scan.nextInt();
    
    for (int i = 0; i <= input; i++) {
      for (int j = 0; j <= input; j++) {
        if (j == i) {
          System.out.print(" ");
        } else if (j == (input - i)) {
        System.out.print(" ");
        } else {
          System.out.print("x"); 
        }
      }
      System.out.println();
    }
  }
}