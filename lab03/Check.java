//Ngan Tran - CSE 002 - Section 210

import java.util.Scanner;

//Document your program. Place your comments here!
//My program helps calculate the amount of cost each person in a group owes based on the input of checkCost, tip percent and the number of people 
//
public class Check{
  //main method required for every Java program
  public static void main (String [] args) {
    
    Scanner myScanner = new Scanner ( System.in);
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    //Input the original cost of the check
    double checkCost = myScanner.nextDouble();
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    double tipPercent = myScanner.nextDouble();
    //Input the tip you intend to pay
    tipPercent /= 100; //We want to convert the percentage into a decimal value
    
    System.out.print("Enter the number of people who went out to dinner: ");
    //Input the number of People
    int numPeople = myScanner.nextInt();
    
    double totalCost;
    double costPerPerson;
    int dollars, //whole dollar amount of cost
        dimes, pennies; //for storing digits to the right of the decimal point for the cost$
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    //get the whole amount, dropping decimal fraction
    dollars = (int)costPerPerson;
    //get dimes amount, e.g.;
    // (int) (6.73 * 10) % 10 -> 67 % 10 -> 7
    // where the % (mod) operator returns the remainder
    // after the division: 583 % 100 -> 83, 27%5 -> 27
    dimes = (int) (costPerPerson * 10) % 10;
    //get pennies amount
    pennies = (int) (costPerPerson * 100) % 10;
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
    
    
    
    
  } // end of main method
} //end of class
  