import java.util.Scanner;

public class CrapsIf{
  /* The program using only if and if-else statements
  involves the rolling of two six sided dice and evaluates
  the results of the roll*/
  public static void main (String [] args) {
    
    Scanner myScanner = new Scanner(System.in);
    /* Ask the user if they'd like randonly cast dice: type 1
    or if they'd like to state the two dice they want to evaluate: type 2.
    */
    int number1 = 1;
    int number2 = 1;
    System.out.print("If you'd like randomly cast dice, please type 1. If you'd like to state the two dice you want to evaluate, please type 2: ");
    int checkMethod = myScanner.nextInt();
    if (checkMethod == 1) {
        number1 = (int) (Math.random()*6 + 1);
        number2 = (int) (Math.random()*6 + 1);
        System.out.println("Your two cast dice are " + number1 +" and " + number2);
    } else if (checkMethod == 2) {
        System.out.print("Please type your first cast dice: ");
        number1 = myScanner.nextInt();
        System.out.print("Please type your second cast dice: ");
        number2 = myScanner.nextInt();
        if (number1 > 0 && number1 < 7) {
        } else {
            System.out.println("Your first cast dice is not available.");
            number1 = myScanner.nextInt();
        }
        if (number2 > 0 && number2 < 7) {
        } else {
            System.out.println("Your second cast dice is not available.");
            number2 = myScanner.nextInt();
        } 
        
   } else {
        System.out.println("Your choice of method is not available.");
        System.out.print("If you'd like randomly cast dice, please type 1. If you'd like to state the two dice you want to evaluate, please type 2: ");
        checkMethod = myScanner.nextInt();
   }
    
    
    int sum = number1 + number2;
    if (sum == 2) {
      System.out.println("Snake Eyes");
    } else if (sum == 3) {
      System.out.println("Ace Deuce");
    } else if (sum == 4) {
          if (number1 == 2) {
            System.out.println("Hard four");
          } else {
            System.out.println("Easy Four");
          }
    } else if (sum == 5) { 
       if (number1 == 1 || number1 == 4) {
          System.out.println("Fever Five");
       } else {
          System.out.println("Fever five");
       }
    } else if (sum == 6) { 
        if (number1 == 2 || number1 == 4) {
          System.out.println("Easy six");
        } else if (number1 == 3) {
            System.out.println("Hard six");
        } else {
            System.out.println("Easy Six");
        }
    } else if (sum == 7) {
        System.out.println("Seven out");
    } else if (sum == 8) {
        if (number1 == 4) {
           System.out.println("Hard Eight");
        } else {
           System.out.println("Easy Eight");
        }
    } else if (sum == 9) {
        System.out.println("Nine");
    } else if (sum == 10) {
        if (number1 == 5) {
          System.out.println("Hard Ten");
        } else {
          System.out.println("Easy Ten");
        }
    } else if (sum == 11) {
        System.out.println("Yo-leven");
    } else if (sum == 12) {
        System.out.println("Boxcars");
    }
     
  }
}