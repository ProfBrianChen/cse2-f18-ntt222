import java.util.Scanner;

public class CrapsSwitch{
  /* The program using only if and if-else statements
  involves the rolling of two six sided dice and evaluates
  the results of the roll*/
  public static void main (String [] args) {
    
    Scanner myScanner = new Scanner(System.in);
    /* Ask the user if they'd like randonly cast dice: type 1
    or if they'd like to state the two dice they want to evaluate: type 2.
    */
    int number1 = 1;
    int number2 = 1;
    System.out.print("If you'd like randomly cast dice, please type 1. If you'd like to state the two dice you want to evaluate, please type 2: ");
    int checkMethod = myScanner.nextInt();
    switch (checkMethod) {
      case 1: 
        number1 = (int) (Math.random()*6 + 1);
        number2 = (int) (Math.random()*6 + 1);
        System.out.println("Your two cast dice are " + number1 +" and " + number2);
        break;
      case 2:
        System.out.print("Please type your first cast dice: ");
        number1 = myScanner.nextInt();
        System.out.print("Please type your second cast dice: ");
        number2 = myScanner.nextInt();
        switch (number1) {
          case 1: 
          case 2:
          case 3:
          case 4:
          case 5:
          case 6:
            break;
          default: 
            System.out.println("Your first cast dice is not available.");
            number1 = myScanner.nextInt();
        } 
        switch (number2) {
          case 1: 
          case 2:
          case 3:
          case 4:
          case 5:
          case 6:
            break;
          default: 
            System.out.println("Your second cast dice is not available.");
            number2 = myScanner.nextInt();
        }
        break;
      default:
        System.out.println("Your choice of method is not available.");
        System.out.print("If you'd like randomly cast dice, please type 1. If you'd like to state the two dice you want to evaluate, please type 2: ");
        checkMethod = myScanner.nextInt();
    }
    
    
    int sum = number1 + number2;
    switch (sum) {
      case 2: System.out.println("Snake Eyes");
        break;
      case 3: System.out.println("Ace Deuce");
        break;
      case 4: 
          switch (number1) {
            case 2: System.out.println("Hard four");
              break;
            default:System.out.println("Easy Four");
              break;
          }
        break;
      case 5: 
        switch (number1) {
          case 1: 
          case 4: System.out.println("Fever Five");
            break;
          default: System.out.println("Fever five");
            break;
        }
      case 6: 
        switch (number1) {
          case 2:
          case 4: System.out.println("Easy six");
            break;
          case 3: System.out.println("Hard six");
            break;
          default: System.out.println("Easy Six");
            break;
        }
      case 7: 
        System.out.println("Seven out");
        break;
      case 8: 
        switch (number1) {
          case 4: System.out.println("Hard Eight");
            break;
          default: System.out.println("Easy Eight");
            break;
        }
        break;
      case 9: 
        System.out.println("Nine");
        break;
      case 10:
        switch (number1) {
          case 5: System.out.println("Hard Ten");
            break;
          default: System.out.println("Easy Ten");
            break;
        }
        break;
      case 11: 
        System.out.println("Yo-leven");
        break;
      case 12:
        System.out.println("Boxcars");
        break;
      default: 
        break;
        
        
        }
    }
  }

  