public class Cyclometer {
  // main method required for every Java program
  
  public static void main(String args[]){
    int secsTrip1 = 480; //the number of seconds for Trip 1561
    int secsTrip2 = 3220;// the number of seconds for Trip 2
    int countsTrip1 = 1561;//the number of counts for Trip 1 
    int countsTrip2 = 9037;//the number of counts for Trip 2
    double distanceTrip1; //the distance of Trip 1
    double distanceTrip2; //the distance of Trip 2
    double totalDistance; //the distance of both Trip 1 and Trip 2
    
    double wheelDiameter=27.0,  //the diameter of a wheel
  	PI=3.14159, // Pi number constant
  	feetPerMile=5280,  //Feet per Mile
  	inchesPerFoot=12,   //Inches per Foot
  	secondsPerMinute=60;  //Seconds Per Minute
    
    System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+" minutes and had "+ countsTrip1+" counts.");
	  System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute)+" minutes and had "+ countsTrip2+" counts.");

    //Above gives distance in inches, for each count, a rotation of the wheel travels the diameter in inches times PI
    distanceTrip1 =  countsTrip1 * wheelDiameter * PI;
    distanceTrip2 =  countsTrip2 * wheelDiameter * PI;
    
    //Gives distance in mile
    distanceTrip1 /= inchesPerFoot * feetPerMile;
    distanceTrip2 /= inchesPerFoot * feetPerMile;
    totalDistance=distanceTrip1+distanceTrip2;
    
    //Print out the output data
    System.out.println("Trip 1 was " + distanceTrip1 + " miles");
    System.out.println("Trip 2 was " + distanceTrip2 + " miles");
    System.out.println("The total distance was "+totalDistance+" miles");
    
    
   
    
    
  } //end of main method  
} //end of class