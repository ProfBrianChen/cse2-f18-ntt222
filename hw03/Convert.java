// Ngan Tran - CSE002 - Section 210

import java.util.Scanner;
/*Program that asks the user for doubles that represent the number of acres 
  of land affected by hurricane precipitation and how many
  inches of rain were dropped on average */

public class Convert {
  
  public static void main (String args[]){
    
    Scanner myScanner = new Scanner (System.in);
    
    // Input the affected area in acres
    System.out.print("Enter the affected area in acres: ");
    double affectedArea_acres = myScanner.nextDouble();
    
    // Input the rainfall in the affected area
    System.out.print("Enter the rainfall in the affected area: ");
    double rainfall_inches = myScanner.nextDouble();
    
    //Convert 1 inch to miles
    double inchesToMiles = 1.57828283 * Math.pow(10, -5.0);
    //Convert 1 acre to square miles
    double acreToSqMiles = 0.0015625;
    
    //Convert affected area in acres to square Miles
    double affectedArea_miles = affectedArea_acres * acreToSqMiles;
    // Convert the rainfall in inches to Miles
    double rainfall_miles = rainfall_inches * inchesToMiles;
    
    // Multiply the affected area with the rainfall in the affected area to get the quantity of rain
    double quantityRain = affectedArea_miles * rainfall_miles;
    //Print out the quantity of rain in inches
    System.out.println( String.format("%.8f",quantityRain) + " cubic miles");
    
    
    
  }
}