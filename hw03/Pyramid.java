//Ngan Tran CSE 002 Section 210

import java.util.Scanner;
// Program that prompts the user for the dimensions of a pyramid and returns the volume inside the pyramid.

public class Pyramid {
  
  public static void main (String arg[]) {
    
    Scanner myScanner = new Scanner(System.in);
    
    //Input the square side of the pyramid
    System.out.print("The square side of the pyramid is (input length): ");
    double sqrPyradmid = myScanner.nextDouble();
    
    //Input the height of the pyramid
    System.out.print("The height of the pyramid is (input height): ");
    double heightPyramid = myScanner.nextDouble();
    
    //The volume inside the pyramid (V = r * r * h * 1/3)
    double volPyramid = sqrPyradmid * sqrPyradmid * heightPyramid * 1/3;
    //Print out the volume of the pyramid
    System.out.println("The volume inside the pyramid is: " + volPyramid);
  }
}