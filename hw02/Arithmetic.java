//Ngan Tran CSE 002 Section 210  
public class Arithmetic {

   public static void main(String args[]){
   
     //Number of pairs of pants
     int numPants = 3;
     //Cost per pair of pants
     double pantsPrice = 34.98;
     
     //Number of sweatshirts
     int numShirts = 2;
     //Cost per shirt
     double shirtPrice = 24.99;
     
     //Number of belts
     int numBelts = 1;
     //cost per belt
     double beltCost = 33.99;
     
     //The tax rate
     double paSalesTax = 0.06;
     
     //Total cost of pants before tax
     double totalCostOfPants = pantsPrice * numPants;
     //Sales tax charged on pants
     double salesTaxPants = totalCostOfPants * paSalesTax;
     
     //Total cost of sweatshirts before tax
     double totalCostOfShirts = shirtPrice * numShirts;
     //Sales tax charged on sweatshirts
     double salesTaxShirts = totalCostOfShirts * paSalesTax;
     
     
     //Total cost of belts before tax
     double totalCostOfBelts = beltCost * numBelts;
     //Sales tax charged on belts
     double salesTaxBelts = totalCostOfBelts * paSalesTax;
     
     //Total cost of each items
     //Display total cost of pants before tax
     System.out.println("The total cost of pants before tax is $" + totalCostOfPants);
     //Display total cost of sweatshirts before tax
     System.out.println("The total cost of sweatshirts before tax is $" + totalCostOfShirts);
     //Display total cost of belts before tax
     System.out.println("The total cost of belts before tax is $" + totalCostOfBelts);
     
     //Sales tax charged buying all of each kind of item
     //Display sales tax charged on pants
     System.out.println("The sales tax charged on pants $" + String.format("%.2f",salesTaxPants));
     //Display sales tax charged on sweatshirts
     System.out.println("The sales tax charged on sweatshirts $" + String.format("%.2f",salesTaxShirts));
     //Display sales tax charged on belts
     System.out.println("The sales tax charged on belts $" + String.format("%.2f",salesTaxBelts));
     
     //Total cost of the purchases before tax
     double totalCostBeforeTax = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
     //Display total cost of the purchases before tax
     System.out.println("The total cost of the purchases before tax is $" + String.format("%.2f",totalCostBeforeTax));
     //Total sales tax
     double totalSalesTax = salesTaxPants + salesTaxShirts + salesTaxBelts;
     //Display total sales tax
     System.out.println("The total sales tax is $" + String.format("%.2f",totalSalesTax));
     //Total cost of the purchases including sales tax
     double totalCosts = totalCostBeforeTax + totalSalesTax;
     //Display total cost of the purchases including sales tax
     System.out.println("The total cost of the purchases including sales tax is $" + String.format("%.2f",totalCosts));
     
     
     
       
     
     
     
     
     
     
     
     
     
     
     
    
   }

}