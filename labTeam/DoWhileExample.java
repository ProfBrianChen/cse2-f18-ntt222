import java.util.Scanner;
public class DoWhileExample{
    public static void main(String [] args){
        Scanner input = new Scanner(System.in);
        int choice = 0; //choice will hold the users selection/input
        // input-- what variables will be needed?
        String name;
        double money;
        String junk;
        int balance = 100;
        double rest;

            System.out.println("Venmo Main Menu");
            System.out.println("1. Send Money");
            System.out.println("2. Request Money");            
            System.out.println("3. Check Balance");
            System.out.println("4. Quit");



        //So far, the do-while loop simply prints out the menu options. What else needs to happen here?
        do{
            choice = input.nextInt();
            //If the user types 1, what should the program do? Use psuedocode or english words in comments
            if (choice == 1) {
                System.out.print("How much do you want to send? ");
                money = input.nextDouble();
                //If the user types 1, what is the expected output? Use psuedocode or english words in comments
                System.out.print("Who do you want to send?");
                name = input.next();
                rest = balance- money;
                System.out.println("You have sent "+ money + " to "+ name + ". You have " + (balance - money) + " in your account.");
                junk = input.nextLine();
            }
            
            //If the user types 2, what should the program do? Use psuedocode or english words in comments
            //If the user types 2, what is the expected output? Use psuedocode or english words in comments
            if (choice == 2) {
                System.out.print("How much do you want to request? ");
                money = input.nextDouble();
                System.out.print("Who do you want to request?");
                name = input.next();
                System.out.println("You have requested "+ money + " to "+ name);
                junk = input.nextLine();
            }

            //If the user types 3, what should the program do? Use psuedocode or english words in comments
            if (choice == 3) {
                System.out.println("You have " + rest + " in your account.");
            }
            
            //if the user types 4, what should the program do? Use psuedocode or english words in comments
            //If the user types 4, what is the expected output? Use psuedocode or english words in comments
            if (choice == 4) {
                return;
            }
        }while(choice != 4);
    }
}