// Ngan Tran - CSE002 - Section 210
// Practice Nested Loop for lab06
import java.util.Scanner;

public class PatternC {
  
  public static void main (String arg[]) {
    
    Scanner input = new Scanner(System.in);
    String junk;
    int num; 
    String cnt ="";
    System.out.print("Please type an integer from 1 to 10: ");
    if (!input.hasNextInt()) {
    System.out.println("Please type an integer from 1 to 10 again: ");
    junk = input.nextLine();
    }
    num = input.nextInt();
    
    for (int i = 1; i <= num; i++) {
      cnt = "";
      for (int j = i; j >= 1; j--) {
        cnt += j;
      }
      System.out.printf("%10s",cnt);
      System.out.println();
    }
  } 
}