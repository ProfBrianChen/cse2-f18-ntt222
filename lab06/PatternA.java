// Ngan Tran - CSE002 - Section 210
// Practice Nested Loop for lab06
import java.util.Scanner;

public class PatternA {
  
  public static void main (String arg[]) {
    
    Scanner input = new Scanner(System.in);
    String junk;
    int num; 
    System.out.print("Please type an integer from 1 to 10: ");
    if (!input.hasNextInt()) {
    System.out.println("Please type an integer from 1 to 10 again: ");
    junk = input.nextLine();
    }
    num = input.nextInt();
    
    for (int i = 1; i <= num; i++) {
      for (int j = 1; j <= i; j++) {
        System.out.print(j + " ");
      }
      System.out.println("");
    }
  } 
}