// Ngan Tran - CSE002 - Section 210
// Practice Nested Loop for lab06
import java.util.Scanner;

public class PatternD {
  
  public static void main (String arg[]) {
    
    Scanner input = new Scanner(System.in);
    String junk;
    int num; 
    System.out.print("Please type an integer from 1 to 10: ");
    if (!input.hasNextInt()) {
    System.out.println("Please type an integer from 1 to 10 again: ");
    junk = input.nextLine();
    }
    num = input.nextInt();
    
    for (int i = num; i >= 1; i--) {
      for (int j = i; j >= 1; j--) {
        System.out.print(j + " ");
      }
      System.out.println("");
    }
  } 
}