// Ngan Tran - CSE 002 - Section 210

import java.util.Scanner;

// Create an integer which helps create a twist.
//A twist is always three lines long, and made of slash characters and X characters.  For example, if length is 25, then your code should print out this:


public class TwistGenerator {

  public static void main (String [] arg) {
    Scanner scan = new Scanner(System.in);
    
    String junkWord;
    System.out.print("Type an integer: ");
    int length;
    
    while (!scan.hasNextInt()) {
      System.out.print("You do not print an integer. Please type an integer: ");
      junkWord = scan.nextLine();
    }
    length = scan.nextInt();
    
    for (int i = 1; i <= length/3; i++) {
      System.out.print("\\ /");
    }
    
    if (length%3 == 1) {
      System.out.print("\\");
    } else if (length%3 == 2) {
      System.out.print("\\ ");
    }
    System.out.println();
    
    for (int i = 1; i <= length/3; i++) {
      System.out.print(" X ");
    }
    System.out.println();
    
    for (int i = 1; i <= length/3; i++) {
      System.out.print("/ \\");
    }
    
    if (length%3 == 1){
      System.out.println("/");
    } else if (length%3 == 2) {
      System.out.println("/ ");
    }
    System.out.println();
    
  }

}